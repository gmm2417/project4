"""Final project, part 1"""
import numpy as np
import time
import matplotlib.pyplot as plt
from m1 import flow as fl #assumes p41.f90 has been compiled with: f2py -c p41.f90 -m m1
from ompp41 import flow as fl1

def jacobi(n,kmax=150000,tol=1.0e-8,s0=0.1,display=False):
    """ Solve liquid flow model equations with
        jacobi iteration.
        Input:
            n: number of grid points in r and theta
            kmax: max number of iterations
            tol: convergence test parameter
            s0: amplitude of cylinder deformation
            display: if True, plots showing the velocity field and boundary deformation
            are generated
        Output:
            w,deltaw: Final velocity field and |max change in w| each iteration
    """

    #-------------------------------------------
    #Set Numerical parameters and generate grid
    Del_t = 0.5*np.pi/(n+1)
    Del_r = 1.0/(n+1)
    Del_r2 = Del_r**2
    Del_t2 = Del_t**2
    r = np.linspace(0,1,n+2)
    t = np.linspace(0,np.pi/2,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation (after dividing by gamma)
    rg2 = rg*rg
    fac = 0.5/(rg2*Del_t2 + Del_r2)
    facp = rg2*Del_t2*fac*(1+0.5*Del_r/rg) #alpha_p/gamma
    facm = rg2*Del_t2*fac*(1-0.5*Del_r/rg) #alpha_m/gamma
    fac2 = Del_r2*fac #beta/gamma
    RHS = fac*(rg2*Del_r2*Del_t2) #1/gamma
    

    #set initial condition/boundary deformation
    w0 = (1-rg**2)/4 #Exact solution when s0=0
    s_bc = s0*np.exp(-10.*((t-np.pi/2)**2))/Del_r
    fac_bc = s_bc/(1+s_bc)

    deltaw = []
    w = w0.copy()
    wnew = w0.copy()

    #Jacobi iteration
    for k in range(kmax):
        #Compute wnew
        wnew[1:-1,1:-1] = RHS[1:-1,1:-1] + w[2:,1:-1]*facp[1:-1,1:-1] + w[:-2,1:-1]*facm[1:-1,1:-1] + (w[1:-1,:-2] + w[1:-1,2:])*fac2[1:-1,1:-1] #Jacobi update

        #Apply boundary conditions
        wnew[:,0] = wnew[:,1] #theta=0
        wnew[:,-1] = wnew[:,-2] #theta=pi/2
        wnew[0,:] = wnew[1,:] #r=0
        wnew[-1,:] = wnew[-2,:]*fac_bc #r=1s

        #Compute delta_p
        deltaw += [np.max(np.abs(w-wnew))]
        w = wnew.copy()
        if k%1000==0: print("k,dwmax:",k,deltaw[k])
        #check for convergence
        if deltaw[k]<tol:
            print("Converged,k=%d,dw_max=%28.16f " %(k,deltaw[k]))
            break

    deltaw = deltaw[:k+1]

    if display==True:
        #plot final velocity field, difference from initial guess, and cylinder
        #surface
        plt.figure()
        plt.contour(t,r,w,50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title('Final velocity field')

        plt.figure()
        plt.contour(t,r,np.abs(w-w0),50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title(r'$|w - w_0|$')

        plt.figure()
        plt.polar(t,np.ones_like(t),'k--')
        plt.polar(t,np.ones_like(t)+s_bc*Del_r,'r-')
        plt.title('Deformed cylinder surface')
        plt.show()

    return w,deltaw



def jacobi_modified(n,kmax=150000,tol=1.0e-8,s0=0.1,display=False):
    """ Solve liquid flow model equations with
        jacobi iteration.
        Input:
            n: number of grid points in r and theta
            kmax: max number of iterations
            tol: convergence test parameter
            s0: amplitude of cylinder deformation
            display: if True, plots showing the velocity field and boundary deformation
            are generated
        Output:
            w,deltaw: Final velocity field and |max change in w| each iteration
    """

    #-------------------------------------------
    #Set Numerical parameters and generate grid
    Del_t = 0.5*np.pi/(n+1)
    Del_r = 1.0/(n+1)
    Del_r2 = Del_r**2
    Del_t2 = Del_t**2
    r = np.linspace(0,1,n+2)
    t = np.linspace(0,np.pi/2,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation (after dividing by gamma)
    rg2 = rg*rg
    fac = 0.5/(rg2*Del_t2 + Del_r2)
    facp = rg2*Del_t2*fac*(1+0.5*Del_r/rg) #alpha_p/gamma
    facm = rg2*Del_t2*fac*(1-0.5*Del_r/rg) #alpha_m/gamma
    fac2 = Del_r2*fac #beta/gamma
    RHS = fac*(rg2*Del_r2*Del_t2) #1/gamma

    #set initial condition/boundary deformation
    w0 = (1-rg**2)/4 #Exact solution when s0=0
    s_bc = s0*np.exp(-10.*((t-np.pi/2)**2))/Del_r
    fac_bc = s_bc/(1+s_bc)

    deltaw = []
    w = w0.copy()
    wnew = w0.copy()

    #Jacobi iteration
    for k in range(kmax):
        #Compute wnew
        wnew[1:-1,1:-1] = RHS[1:-1,1:-1] + w[2:,1:-1]*facp[1:-1,1:-1] + w[:-2,1:-1]*facm[1:-1,1:-1] + (w[1:-1,:-2] + w[1:-1,2:])*fac2[1:-1,1:-1] #Jacobi update

        #Apply boundary conditions
        wnew[:,0] = wnew[:,1] #theta=0
        wnew[:,-1] = wnew[:,-2] #theta=pi/2
        wnew[0,:] = wnew[1,:] #r=0
        wnew[-1,:] = wnew[-2,:]*fac_bc #r=1s

        #Compute delta_p
        deltaw += [np.max(np.abs(w-wnew))]
        w = wnew.copy()
        
        #check for convergence
        if deltaw[k]<tol:
            
            break

    deltaw = deltaw[:k+1]

    if display:
        #plot final velocity field, difference from initial guess, and cylinder
        #surface
        plt.figure()
        plt.contour(t,r,w,50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title('Final velocity field')

        plt.figure()
        plt.contour(t,r,np.abs(w-w0),50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title(r'$|w - w_0|$')

        plt.figure()
        plt.polar(t,np.ones_like(t),'k--')
        plt.polar(t,np.ones_like(t)+s_bc*Del_r,'r-')
        plt.title('Deformed cylinder surface')

    return k

def performance(n,display=False):
    """Analyze performance of codes
    Add input/output variables as needed.
    """
    fl.numthreads=4
    n=np.linspace(10,100,10)
    Time=np.zeros(10)
    Time2=np.zeros(10)
    Time3=np.zeros(10)
    Time4=np.zeros(10)
    Conv1=np.zeros(10)
    Conv2=np.zeros(10)
    Conv3=np.zeros(10)
    for j in range(10):
    	t1=time.time()
    	w1=fl.sgisolve((j+1)*10)  
    	t2=time.time()
    	Time[j]=t2-t1	    
			    
    	t3=time.time()
    	w2=fl.jacobi((j+1)*10)
    	t4=time.time()
    	Time2[j]=t4-t3

    	t5=time.time()
    	w3=fl1.sgisolve((j+1)*10)
    	t6=time.time()
    	Time3[j]=t6-t5

    	t7=time.time()
    	w4=jacobi((j+1)*10)
    	t8=time.time()
    	Time4[j]=t8-t7

    	l=fl.sgisolve_modified((j+1)*10)
    	Conv1[j]=l

    	
    	k=jacobi_modified((j+1)*10)
    	Conv3[j]=k

     	
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.plot(n, Time, label='sgi')
    ax.plot(n, Time2, label='jacobi fortran')
    ax.plot(n, Time3, label='sgi omp')
    ax.plot(n, Time4, label='jacobi python')
    plt.title('TIME')
    plt.xlabel('n')
    plt.ylabel('time(s)')
    ax.legend()
    plt.show()

    fig2 = plt.figure()
    ax = plt.subplot(111)
    ax.plot(n, Conv1, label='exit value sgi')
    ax.plot(n,Conv3, label='exit value jacobi python')
    plt.title('exit value versus n')
    ax.legend()
    plt.show()

    n=np.linspace(10,60,11)
    Error=np.zeros(11)
    for j in range(11):
    	w1=fl.sgisolve(j*5+10)
    	w2=fl.jacobi(j*5+10)
    	w=np.zeros((j*5+12)**2)
    	for i in range(0,j*5+12):
        	w[i*(j*5+12):(i+1)*(j*5+12)]=w2[i,:]
    	Error[j]=max(abs(w-w1))


    fig3 = plt.figure()
    ax = plt.subplot(111)
    ax.plot(n,Error, label='Error')
    plt.title('Error for the w for Jacobi and Sgi')
    ax.legend()
    plt.show()
    
    
    
    return None



if __name__=='__main__':
    #Add code below to call performance
    #and generate figures you are submitting in
    #your repo.
    """input_p = []
    output_p=input_p.append(performance(input_p,display=True))"""


    input_r = []
    output_r=input_r.append(jacobi(30,kmax=150000,tol=1.0e-8,s0=0.1,display=True))
