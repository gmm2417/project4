!Final project part 1
!Module for flow simulations of liquid through tube
!This module contains a few module variables (see comments below)
!and four subroutines:
!jacobi: Uses jacobi iteration to compute solution
! to flow through tube
!sgisolve: To be completed. Use sgi method to
! compute flow through tube
!mvec: To be completed; matrix-vector multiplication z = Ay
!mtvec: To be completed; matrix-vector multiplication z = A^T y
module flow

use omp_lib
    implicit none
    real(kind=8), parameter :: pi = acos(-1.d0)
    integer :: numthreads !number of threads used in parallel regions
    integer :: fl_kmax=150000 !max number of iterations
    real(kind=8) :: fl_tol=0.00000001d0 !convergence criterion
    real(kind=8), allocatable, dimension(:) :: fl_deltaw !|max change in w| each iteration
    real(kind=8) :: fl_s0=0.1d0 !deformation magnitude

contains
!-----------------------------------------------------
!Solve 2-d tube flow problem with Jacobi iteration
subroutine jacobi(n,w)
    !input  n: number of grid points (n+2 x n+2) grid
    !output w: final velocity field
    !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
    !A number of module variables can be set in advance.

    integer, intent(in) :: n
    real(kind=8), dimension(0:n+1,0:n+1), intent(out) :: w
    integer :: i1,j1,k1
    real(kind=8) :: del_r,del_t,del_r2,del_t2
    real(kind=8), dimension(0:n+1) :: s_bc,fac_bc
    real(kind=8), dimension(0:n+1,0:n+1) :: r,r2,t,RHS,w0,wnew,fac,fac2,facp,facm

    if (allocated(fl_deltaw)) then
      deallocate(fl_deltaw)
    end if
    allocate(fl_deltaw(fl_kmax))


    !grid--------------
    del_t = 0.5d0*pi/dble(n+1)
    del_r = 1.d0/dble(n+1)
    del_r2 = del_r**2
    del_t2 = del_t**2


    do i1=0,n+1
        r(i1,:) = i1*del_r
    end do

    do j1=0,n+1
        t(:,j1) = j1*del_t
    end do
    !-------------------

    !Update-equation factors------
    r2 = r**2
    fac = 0.5d0/(r2*del_t2 + del_r2)
    facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
    facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma
    fac2 = del_r2 * fac !beta/gamma
    RHS = fac*(r2*del_r2*del_t2) !1/gamma
    !----------------------------

    !set initial condition/boundary deformation
    w0 = (1.d0-r2)/4.d0
    w = w0
    wnew = w0
    s_bc = fl_s0*exp(-10.d0*((t(0,:)-pi/2.d0)**2))/del_r
    fac_bc = s_bc/(1.d0+s_bc)


    !Jacobi iteration
    do k1=1,fl_kmax
        wnew(1:n,1:n) = RHS(1:n,1:n) + w(2:n+1,1:n)*facp(1:n,1:n) + w(0:n-1,1:n)*facm(1:n,1:n) + &
                                         (w(1:n,0:n-1) + w(1:n,2:n+1))*fac2(1:n,1:n)

        !Apply boundary conditions
        wnew(:,0) = wnew(:,1) !theta=0
        wnew(:,n+1) = wnew(:,n) !theta=pi/2
        wnew(0,:) = wnew(1,:) !r=0
        wnew(n+1,:) = wnew(n,:)*fac_bc !r=1s

        fl_deltaw(k1) = maxval(abs(wnew-w)) !compute relative error

        w=wnew    !update variable
        if (fl_deltaw(k1)<fl_tol) exit !check convergence criterion
        if (mod(k1,1000)==0) print *, k1,fl_deltaw(k1)
    end do

    print *, 'k,error=',k1,fl_deltaw(min(k1,fl_kmax))

end subroutine jacobi
!-----------------------------------------------------
subroutine sgisolve(n,w)
!Solve 2-d tube flow problem with sgi method
  !input  n: number of grid points (n+2 x n+2) grid
  !output w: final velocity field stored in a column vector
  !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
  !A number of module variables can be set in advance.

  !input  n: number of grid points (n+2 x n+2) grid
  !output w: final velocity field stored in a column vector
  !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
  !A number of module variables can be set in advance.
  integer, intent(in) :: n
  real(kind=8), dimension((n+2)*(n+2)), intent(out) :: w
  real(kind=8), dimension((n+2)*(n+2)) :: v,d,e,ad,md,enew,wnew,b,dnew
  real(kind=8), dimension(0:n+1) :: s_bc,fac_bc
  real(kind=8), dimension(0:n+1,0:n+1) :: r,r2,t,RHS,w0,fac,fac2,facp,facm
  real(kind=8), dimension(n+2) :: faci,facmi,fac2i,facpi,faci_bc
  real(kind=8) :: del_t,del_r,kapa,del_t2,del_r2,mu,dot_enew,dot_e
  integer :: l,i1,j1,i2,i3
  !add other variables as needed
dot_e=0.d0
dot_enew=0.d0
  !grid spacings------------

  del_t = 0.5d0*pi/dble(n+1)
  del_r = 1.d0/dble(n+1)
  del_r2 = del_r**2
  del_t2 = del_t**2

!$ CALL omp_set_num_threads(numthreads)
    do i1=1,n+2
        r(i1-1,:) = (i1-1)*del_r
    end do

    do j1=1,n+2
        t(:,j1-1) = (j1-1)*del_t
    end do
    !-------------------

    !Update-equation factors------
    r2 = r**2
    fac = 0.5d0/(r2*del_t2 + del_r2)
    facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
    facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma
    fac2 = del_r2 * fac !beta/gamma
    RHS = fac*(r2*del_r2*del_t2) !1/gamma

    !----------------------------

   
    s_bc = fl_s0*exp(-10.d0*((t(1,:)-pi/2.d0)**2))/del_r
    fac_bc = s_bc/(1.d0+s_bc)

    faci=fac(:,1)
    facpi=facp(:,1)
    facmi=facm(:,1)
    fac2i=fac2(:,1)
    faci_bc=fac_bc

 
    b=0.d0
    b(1:n+3)=0.d0
   
   do i2=1,n
        b(i2*(n+2)+2:i2*(n+2)+n+1)=-RHS(i2,1)
        b((i2+1)*(n+2))=0.d0
        b((i2+1)*(n+2)+1)=0.d0
    end do
    
    b((n+1)*(n+2)+1:)=0.d0

    
  
    if (allocated(fl_deltaw)) then
      deallocate(fl_deltaw)
    end if
    allocate(fl_deltaw(fl_kmax))

!Initial guess
w=0.d0


    call mtvec(n,faci,fac2i,facpi,facmi,faci_bc,b,v)
   

    d=v
    
    e=v
    
     
    do l=1,fl_kmax
       call mvec(n,faci,fac2i,facpi,facmi,faci_bc,d,ad)
      !print *, 'sum ad=', sum(ad)
       call mtvec(n,faci,fac2i,facpi,facmi,faci_bc,ad,md)
       !print *, 'md=', sum(md)
       kapa=(dot_product(e,e))/(dot_product(d,md))
       !print *, 'kapa=', kapa
       wnew=w+kapa*d
       !print *, kapa*d
       enew=e+(-1.d0)*kapa*md
       !$OMP PARALLEL DO REDUCTION(+:dot_e)
       do i2=1,(n+2)**2
       dot_e=dot_e+e(i2)**2
       end do
       !$OMP END PARALLEL DO
       !$OMP PARALLEL DO REDUCTION(+:dot_enew)
       do i3=1,(n+2)**2
       dot_enew=dot_enew+enew(i3)**2
       end do
       !$OMP END PARALLEL DO
       mu=dot_enew/dot_e
       dot_e=0.d0
       dot_enew=0.d0
       dnew=enew+mu*d
       e=enew
       fl_deltaw(l) = maxval(abs(wnew-w))
       !print *, 'tol=', fl_deltaw(l)
       w=wnew
       d=dnew
        
       !print *, d
       if (fl_deltaw(l)<fl_tol) exit
          
       
    end do
print *, 'l=', l
    

end subroutine sgisolve


!Compute matrix-vector multiplication, z = Ay
subroutine mvec(n,fac,fac2,facp,facm,fac_bc,y,z)
    !input n: grid is (n+2) x (n+2)
    ! fac,fac2,facp,facm,fac_bc: arrays that appear in
    !   discretized equations
    ! y: vector multipled by A
    !output z: result of multiplication Ay
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n+2), intent(in) :: fac,fac2,facp,facm,fac_bc
    real(kind=8), dimension((n+2)*(n+2)), intent(in) :: y
    real(kind=8), dimension((n+2)*(n+2)), intent(out) :: z
    real(kind=8) :: del_r,del_t
    integer :: i1,j1,k1,k2


    del_t = 0.5d0*pi/dble(n+1)
    del_r = 1.d0/dble(n+1)
    !add other variables as needed


 do i1=1,n+2
     z(i1)=(-1.d0)*(y(i1)-y(i1+n+2))
     z((n+2)*(n+1)+i1)=(-fac_bc(i1))*y((n+2)*(n)+i1)+y((n+2)*(n+1)+i1)
     
  end do

  do j1=1,n-1
     !z(j1*(n+2)+j1)=((-1.d0)/del_t)*y(j1*(n+3))+((1.d0)/del_t)*y(j1*(n+3)+1)
     z((j1+1)*(n+2))=(-1.d0)*y((j1+1)*(n+2)-1)+(1.d0)*y((j1+1)*(n+2))
     z((j1+1)*(n+2)+1)=(-1.d0)*y((j1+1)*(n+2)+1)+(1.d0)*y((j1+1)*(n+2)+2)
  end do
  z(n+3)=(-1.d0)*y((n+3))+(1.d0)*y((n+4))
  z((n+1)*(n+2))=(-1.d0)*y((n+1)*(n+2)-1)+(1.d0)*y((n+1)*(n+2))
  do k1=1,n
     do k2=1,n
        z(k1*(n+2)+1+k2)=facm(k1+1)*y((k1-1)*(n+2)+k2+1)+fac2(k1+1)*(y(k1*(n+2)+k2)+y(k1*(n+2)+k2+2))            
        z(k1*(n+2)+1+k2)=z(k1*(n+2)+1+k2)+(-1.d0)*y(k1*(n+2)+k2+1)+facp(k1+1)*y((k1+1)*(n+2)+k2+1)
     end do
  end do

end subroutine mvec


!Compute matrix-vector multiplication, z = A^T y
subroutine mtvec(n,fac,fac2,facp,facm,fac_bc,y,z)
    !input n: grid is (n+2) x (n+2)
    ! fac,fac2,facp,facm,fac_bc: arrays that appear in
    !   discretized equations
    ! y: vector multipled by A^T
    !output z: result of multiplication A^T y
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n+2), intent(in) :: fac,fac2,facp,facm,fac_bc
    real(kind=8), dimension((n+2)*(n+2)), intent(in) :: y
    real(kind=8), dimension((n+2)*(n+2)), intent(out) :: z
    integer :: i1,i2,i3,j1,j2,j3,j4
    real(kind=8) :: del_t,del_r
    

    del_t = 0.5d0*pi/dble(n+1)
    del_r = 1.d0/dble(n+1)
    !add other variables as needed

   z(1)=(-1.d0)*y(1)
    z(n+3)=(1.d0)*y(1)+(-1.d0)*y(n+3)+fac2(2)*y(n+4)
    z(n+4)=(1.d0)*y(2)+(1.d0)*y(n+3)-1.d0*y(n+4)+fac2(2)*y(n+5)+facm(3)*y(2*n+6)
    z(n+2)=(-1.d0)*y(n+2)
    z(2*n+3)=(1.d0)*y(n+1)+(-1.d0)*y(2*n+4)+(-1.d0)*y(2*n+3)+fac2(2)*y(2*n+2)+facm(3)*y(3*n+5) 
    z(2*(n+2))=(1.d0)*y(n+2)+(1.d0)*y(2*n+4)+fac2(2)*y(2*n+3) 
    
    do i1=1,n
       z(i1+1)=(-1.d0)*y(i1+1)+facm(2)*y(n+3+i1)

    end do
    
    do j1=1,n-2
      z(n+4+j1)=(1.d0)*y(j1+2)+fac2(2)*(y(n+3+j1)+y(n+5+j1))-1.d0*y(n+4+j1)+facm(3)*y(2*n+6+j1)

    end do
    
    !First two and last row of the n-2 middle blocks
    do j2=1,n-2
       z((j2+1)*(n+2)+1)=(-1.d0)*y((j2+1)*(n+2)+1)+fac2(j2+2)*y((j2+1)*(n+2)+2)
       z((j2+1)*(n+2)+2)=facp(j2+1)*y(j2*(n+2)+2)+(1.d0)*y((j2+1)*(n+2)+1)+(-1.d0)*y((j2+1)*(n+2)+2)
       z((j2+1)*(n+2)+2)=z((j2+1)*(n+2)+2)+fac2(j2+2)*y((j2+1)*(n+2)+3)+facm(j2+3)*y((j2+2)*(n+2)+2)
       z((j2+2)*(n+2))=fac2(j2+2)*y((j2+2)*(n+2)-1)+(1.d0)*y((j2+2)*(n+2))
       z((j2+2)*(n+2)-1)=facp(j2+1)*y((j2+1)*(n+2)-1)+fac2(j2+2)*y((j2+2)*(n+2)-2)+(-1.d0)*y((j2+2)*(n+2)-1)
       z((j2+2)*(n+2)-1)=z((j2+2)*(n+2)-1)+(-1.d0)*y((j2+2)*(n+2))+facm(j2+3)*y((j2+3)*(n+2)-1)
    end do

   ! if (n>1) then
    !First two and last row of the n+1 block and the last block
    z(n*(n+2)+1)=(-1.d0)*(y(n*(n+2)+1))+fac2(n+1)*y(n*(n+2)+2)+(-1.d0)*fac_bc(1)*y((n+1)*(n+2)+1)
    z((n+1)*(n+2))=(1.d0)*y((n+1)*(n+2))+fac2(n+1)*y((n+1)*(n+2)-1)+(-1.d0)*fac_bc(n+2)*y((n+2)*(n+2))
!print *, 'fac2i', faci_bc
!print *, 'z=', (1.d0/del_t)*y((n+1)*(n+2))+fac2i(n+1)*y((n+1)*(n+2)-1)

! if (n>1) then
    z(n*(n+2)+2)=facp(n)*y((n-1)*(n+2)+2)+(1.d0)*y(n*(n+2)+1)+(-1.d0)*y((n)*(n+2)+2)
    z(n*(n+2)+2)=z(n*(n+2)+ 2)+fac2(n+1)*y((n)*(n+2)+3)-fac_bc(2)*y((n+1)*(n+2)+2)
!end if

    z((n+1)*(n+2)-1)=facp(n)*y(n*(n+2)-1)+fac2(n+1)*y((n+1)*(n+2)-2)+(-1.d0)*y((n+1)*(n+2)-1)
    z((n+1)*(n+2)-1)=z((n+1)*(n+2)-1)+(-1.d0)*fac_bc(n+1)*y((n+2)*(n+2)-1)+(-1.d0)*y((n+1)*(n+2))
   ! end if
!First and last line of the final block
    z((n+1)*(n+2)+1)=y((n+1)*(n+2)+1)
    z((n+2)*(n+2))=y((n+2)*(n+2))
    
    !print *, z
    do j3=2,n-1
        do j4=3,n
           z(j3*(n+2)+j4)=facp(j3)*y((j3-1)*(n+2)+j4)+fac2(j3+1)*(y(j3*(n+2)+j4-1)+y(j3*(n+2)+j4+1))
           z(j3*(n+2)+j4)=z(j3*(n+2)+j4)+(-1.d0)*y(j3*(n+2)+j4)+facm(j3+2)*y((j3+1)*(n+2)+j4)

        end do

   end do

do i2=3,n
      z(n*(n+2)+i2)=facp(n)*y((n-1)*(n+2)+i2)+fac2(n+1)*(y(n*(n+2)+i2-1)+y(n*(n+2)+i2+1))
         z(n*(n+2)+i2)=z(n*(n+2)+i2)+(-1.d0)*y(n*(n+2)+i2)-fac_bc(i2)*y((n+1)*(n+2)+i2)
   end do
   !print *, 'z=', z
   !second line to n+1 line of last block
   do i3=2,n+1
      z((n+1)*(n+2)+i3)=facp(n+1)*y(n*(n+2)+i3)+y((n+1)*(n+2)+i3)
      
   end do  
   


end subroutine mtvec

subroutine sgisolve_modified(n,l)
!Solve 2-d tube flow problem with sgi method
  !input  n: number of grid points (n+2 x n+2) grid
  !output w: final velocity field stored in a column vector
  !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
  !A number of module variables can be set in advance.

  !input  n: number of grid points (n+2 x n+2) grid
  !output w: final velocity field stored in a column vector
  !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
  !A number of module variables can be set in advance.
  integer, intent(in) :: n
  real(kind=8), dimension((n+2)*(n+2)) :: w
  real(kind=8), dimension((n+2)*(n+2)) :: v,d,e,ad,md,enew,wnew,b,dnew
  real(kind=8), dimension(0:n+1) :: s_bc,fac_bc
  real(kind=8), dimension(0:n+1,0:n+1) :: r,r2,t,RHS,w0,fac,fac2,facp,facm
  real(kind=8), dimension(n+2) :: faci,facmi,fac2i,facpi,faci_bc
  real(kind=8) :: del_t,del_r,kapa,del_t2,del_r2,mu,dot_enew,dot_e
  integer :: i1,j1,i2,i3
  integer, intent(out) :: l
  !add other variables as needed
dot_e=0.d0
dot_enew=0.d0
  !grid spacings------------

  del_t = 0.5d0*pi/dble(n+1)
  del_r = 1.d0/dble(n+1)
  del_r2 = del_r**2

  del_t2 = del_t**2

!$ CALL omp_set_num_threads(numthreads)
    do i1=1,n+2
        r(i1-1,:) = (i1-1)*del_r
    end do

    do j1=1,n+2
        t(:,j1-1) = (j1-1)*del_t
    end do
    !-------------------

    !Update-equation factors------
    r2 = r**2
    fac = 0.5d0/(r2*del_t2 + del_r2)
    facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
    facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma
    fac2 = del_r2 * fac !beta/gamma
    RHS = fac*(r2*del_r2*del_t2) !1/gamma

    !----------------------------

   
    s_bc = fl_s0*exp(-10.d0*((t(1,:)-pi/2.d0)**2))/del_r
    fac_bc = s_bc/(1.d0+s_bc)

    faci=fac(:,1)
    facpi=facp(:,1)
    facmi=facm(:,1)
    fac2i=fac2(:,1)
    faci_bc=fac_bc

 
    b=0.d0
    b(1:n+3)=0.d0
   
   do i2=1,n
        b(i2*(n+2)+2:i2*(n+2)+n+1)=-RHS(i2,1)
        b((i2+1)*(n+2))=0.d0
        b((i2+1)*(n+2)+1)=0.d0
    end do
    
    b((n+1)*(n+2)+1:)=0.d0

    
  
    if (allocated(fl_deltaw)) then
      deallocate(fl_deltaw)
    end if
    allocate(fl_deltaw(fl_kmax))

!Initial guess
w=0.d0


    call mtvec(n,faci,fac2i,facpi,facmi,faci_bc,b,v)
   

    d=v
    
    e=v
    
     
    do l=1,fl_kmax
       call mvec(n,faci,fac2i,facpi,facmi,faci_bc,d,ad)
      !print *, 'sum ad=', sum(ad)
       call mtvec(n,faci,fac2i,facpi,facmi,faci_bc,ad,md)
       !print *, 'md=', sum(md)
       kapa=(dot_product(e,e))/(dot_product(d,md))
       !print *, 'kapa=', kapa
       wnew=w+kapa*d
       !print *, kapa*d
       enew=e+(-1.d0)*kapa*md
       !$OMP PARALLEL DO REDUCTION(+:dot_e)
       do i2=1,(n+2)**2
       dot_e=dot_e+e(i2)**2
       end do
       !$OMP END PARALLEL DO
       !$OMP PARALLEL DO REDUCTION(+:dot_enew)
       do i3=1,(n+2)**2
       dot_enew=dot_enew+enew(i3)**2
       end do
       !$OMP END PARALLEL DO
       mu=dot_enew/dot_e
       dot_e=0.d0
       dot_enew=0.d0
       dnew=enew+mu*d
       e=enew
       fl_deltaw(l) = maxval(abs(wnew-w))
       !print *, 'tol=', fl_deltaw(l)
       w=wnew
       d=dnew
        
       !print *, d
       if (fl_deltaw(l)<fl_tol) exit
          
       
    end do

    

end subroutine sgisolve_modified


subroutine jacobi_modified(n,k1)
    !input  n: number of grid points (n+2 x n+2) grid
    !output w: final velocity field
    !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
    !A number of module variables can be set in advance.

    integer, intent(in) :: n
    real(kind=8), dimension(0:n+1,0:n+1) :: w
    integer :: i1,j1
    integer, intent(out) :: k1
    real(kind=8) :: del_r,del_t,del_r2,del_t2
    real(kind=8), dimension(0:n+1) :: s_bc,fac_bc
    real(kind=8), dimension(0:n+1,0:n+1) :: r,r2,t,RHS,w0,wnew,fac,fac2,facp,facm

    if (allocated(fl_deltaw)) then
      deallocate(fl_deltaw)
    end if
    allocate(fl_deltaw(fl_kmax))


    !grid--------------
    del_t = 0.5d0*pi/dble(n+1)
    del_r = 1.d0/dble(n+1)
    del_r2 = del_r**2
    del_t2 = del_t**2


    do i1=0,n+1

        r(i1,:) = i1*del_r
    end do

    do j1=0,n+1
        t(:,j1) = j1*del_t
    end do
    !-------------------

    !Update-equation factors------
    r2 = r**2
    fac = 0.5d0/(r2*del_t2 + del_r2)
    facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
    facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma
    fac2 = del_r2 * fac !beta/gamma
    RHS = fac*(r2*del_r2*del_t2) !1/gamma
    !----------------------------

    !set initial condition/boundary deformation
    w0 = (1.d0-r2)/4.d0
    w = w0
    wnew = w0
    s_bc = fl_s0*exp(-10.d0*((t(0,:)-pi/2.d0)**2))/del_r
    fac_bc = s_bc/(1.d0+s_bc)


    !Jacobi iteration
    do k1=1,fl_kmax
        wnew(1:n,1:n) = RHS(1:n,1:n) + w(2:n+1,1:n)*facp(1:n,1:n) + w(0:n-1,1:n)*facm(1:n,1:n) + &
                                         (w(1:n,0:n-1) + w(1:n,2:n+1))*fac2(1:n,1:n)

        !Apply boundary conditions
        wnew(:,0) = wnew(:,1) !theta=0
        wnew(:,n+1) = wnew(:,n) !theta=pi/2
        wnew(0,:) = wnew(1,:) !r=0
        wnew(n+1,:) = wnew(n,:)*fac_bc !r=1s

        fl_deltaw(k1) = maxval(abs(wnew-w)) !compute relative error

        w=wnew    !update variable
        if (fl_deltaw(k1)<fl_tol) exit !check convergence criterion
        if (mod(k1,1000)==0) print *, k1,fl_deltaw(k1)
    end do

    print *, 'k,error=',k1,fl_deltaw(min(k1,fl_kmax))

end subroutine jacobi_modified



end module flow

